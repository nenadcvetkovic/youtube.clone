@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-default">
                <div class="card-header">Channel Settings</div>

                <div class="card-body">

                    <form action="/channel/{{ $channel->slug }}/edit" method="post" enctype="multipart/form-data">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') ? old('name') : $channel->name }}">
                        </div>

                        @if ($errors->has('name'))
                            <div class="help-block alert alert-danger">
                                {{ $errors->first('name')}}
                            </div>
                        @endif

                        <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                            <label for="slug">Slug</label>

                            <div class="input-group">
                                <div class="input-group-addon">{{ config('app.url').'/channel/' }}</div>
                                <input type="text" class="form-control" id="slug" name="slug" value="{{ old('slug') ? old('slug') : $channel->slug }}">
                            </div>
                        </div>
                        @if ($errors->has('slug'))
                            <div class="help-block alert alert-danger">
                                {{ $errors->first('slug')}}
                            </div>
                        @endif

                        <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description" id="description">{{ old('description') ? old('description') : $channel->description }}</textarea>
                        </div>
                        @if ($errors->has('description'))
                            <div class="help-block alert alert-danger">
                                {{ $errors->first('description')}}
                            </div>
                        @endif
        

                        <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                            <label for="image">Channel image</label>
                            <input type="file" class="form-control" id="image" name="image">
                        </div>

                        <button class="btn btn-primary" type="submit">Update</button>

                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
