<?php

namespace App\Models;

use App\Models\Channel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'uid',
        'title',
        'description',
        'video_filename',
        'video_id',
        'processed',
        'visibility',
        'allow_votes',
        'allow_comments',
        'processed_percentage',
        'form',
        'filesize',
    ];

    /**
    * channel
    */
    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }


    /**
    * getRouteKeyName
    */
    public function getRouteKeyName()
    {
        return 'uid';
    }

    /**
    * scopeLatestFirst
    */
    public function scopeLatestFirst($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
    * isProcessed
    */
    public function isProcessed()
    {
        return $this->processed;
    }

    /**
    * getThumbnail
    */
    public function getThumbnail()
    {
        
        
    }
}
