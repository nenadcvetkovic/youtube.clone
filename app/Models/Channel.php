<?php

namespace App\Models;

use App\Models\Video;
use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $fillable = [
        'uid',
        'name',
        'slug',
        'description',
        'image_filename',
    ];

    /**
    * user
    */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
    * getRouteKeyName
    */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
    * videos
    */
    public function videos()
    {
        return $this->hasMany(Video::class);
    }
}
