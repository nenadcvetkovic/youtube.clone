<?php

namespace App\Policies;

use App\Models\Channel;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ChannelPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
    * update
    */
    public function store(User $user, Channel $channel)
    {
        return $user->id === $channel->user_id;
    }


    /**
    * update
    */
    public function update(User $user, Channel $channel)
    {
        return $user->id === $channel->user_id;
    }

    /**
    * update
    */
    public function edit(User $user, Channel $channel)
    {
        return $user->id === $channel->user_id;
    }
}
